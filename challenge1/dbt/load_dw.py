
import os
import sys
import argparse
import logging
import psycopg2
import psycopg2.extras
import pandas as pd
from io import StringIO


def _get_config(argv):
    parser = argparse.ArgumentParser()
    parser.add_argument('--host', dest='host', type=str)
    parser.add_argument('--port', dest='port', type=int)
    parser.add_argument('--username', dest='username', type=str)
    parser.add_argument('--password', dest='password', type=str)
    parser.add_argument('--database', dest='database', type=str)
    parser.add_argument('--table', dest='table', type=str)
    parser.add_argument('--source', dest='source', type=str)
    parser.add_argument('--delimiter', dest='delimiter', type=str, default=',')
    config, _ = parser.parse_known_args(argv)

    return config


def _connect_db(config):
    """Create database connection

    :param config Configuration parameters
    """
    dbname=config.database
    user = config.username
    password = config.password
    host = config.host
    conn = psycopg2.connect(
        dbname=dbname, 
        user=user, 
        password=password,
        host=host
    )

    cursor = conn.cursor(cursor_factory=psycopg2.extras.RealDictCursor)

    return conn, cursor

def _get_source_path(source):
    """Build absolute source file path

    :param source Source path
    """
    current_path = os.path.dirname(os.path.abspath(__file__))
    return os.path.join(current_path, source)


def main(argv):
    try:
        config = _get_config(argv)
        delimiter = config.delimiter
        table = config.table
        conn, cursor = _connect_db(config)
        source_path = _get_source_path(config.source)
        if table == "flights":
            buffer = StringIO()
            frame = pd.read_csv(source_path)
            frame = frame.drop(frame.columns[0], axis=1)
            columns = [
            "carrier",
            "flight",
            "year",
            "month",
            "day",
            "hour",
            "minute",
            "actual_dep_time",
            "sched_dep_time",
            "dep_delay",
            "actual_arr_time",
            "sched_arr_time",
            "arr_delay",
            "tailnum",
            "origin",
            "dest",
            "air_time",
            "distance",
            "time_hour"
            ]

            frame = frame[columns]
            frame.info()
            print(frame["carrier"].unique())
            frame.to_csv("test.csv",index=False)
            source_path = "test.csv"
        elif table == "weather":
            frame = pd.read_csv(source_path)
            frame = frame.drop_duplicates(subset=["origin","year","month","day","hour"])
            frame.info()
            frame = frame.astype(str)
            frame.replace("nan","NA",inplace=True)
            frame.replace("","NA",inplace=True)
            print(frame["wind_gust"].unique())
            frame.to_csv("test.csv",index=False)
            source_path = "test.csv"

        try:
            with open(source_path, 'r') as f:
                next(f)
                if table == "planes" or table == "weather":
                    cursor.copy_from(f,table,sep=delimiter,null="NA")
                else:
                    cursor.copy_from(f,table,sep=delimiter)
            conn.commit()
        finally:
            cursor.close()
            conn.close()
    except Exception as e:
        logging.exception(e)


if __name__ == '__main__':
    logging.getLogger().setLevel(logging.INFO)
    main(sys.argv)
