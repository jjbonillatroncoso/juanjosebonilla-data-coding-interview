#! /usr/bin/env bash

cd challenge1

docker run \
--name de_challenge \
-e POSTGRES_PASSWORD="Password1234**" \
-e POSTGRES_DB=dw_flights \
-v ${PWD}/init_db.sql:/docker-entrypoint-initdb.d/init_db.sql \
-p 5432:5432 \
-d postgres

sleep 3

cd ..

python challenge1/dbt/load_dw.py  --host localhost --port 5432 --username postgres --password Password1234** --database dw_flights --source dataset/nyc_airlines.csv --table airlines 
python challenge1/dbt/load_dw.py  --host localhost --port 5432 --username postgres --password Password1234** --database dw_flights --source dataset/nyc_airports.csv --table airports
python challenge1/dbt/load_dw.py  --host localhost --port 5432 --username postgres --password Password1234** --database dw_flights --source dataset/nyc_planes.csv --table planes 
python challenge1/dbt/load_dw.py  --host localhost --port 5432 --username postgres --password Password1234** --database dw_flights --source dataset/nyc_flights.csv --table flights  
python challenge1/dbt/load_dw.py  --host localhost --port 5432 --username postgres --password Password1234** --database dw_flights --source dataset/nyc_weather.csv --table weather 

  